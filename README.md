# First  exercice
Here we have two datasets which contains information that we can match together , but before we should clean our data , how ?

1. Missing values :

can be a big hurdle in achieving our goals and extracting important information from both tables.
for example if we look for the observations that have no experience , we will have several values ​​to NAN, so in my opinion, it would be good to concederate them as having experience equal to 0 , this logic that can not be done in all other problems, but it is appropriate to the context.

Usually we can replace a missing value by the average , in other cases we can delete these observations which contains missing values but as I say that depend of the number of this NAN .
We can also detect several error cases that we can have in our datasets, for example  we can check if the end date is after the start date not the opposite .

2. the best resume ??

how to choose the best profile?
in order to answer this question we can use several methods, the first and the simplest that comes to mind for a computer scientist is PostgreSQL .
we can program some functions or procedure using SQL , sing PL / PGSQL based on sql queries, it's not very complicated to search the profiles which are suitable for a job offer,
especially since we have some columns that have the same information , for example if the candidate wishes to apply for an offer where there will be to travel or not,
and on the other hand owe can see if the offer requires a displacement or not .
Then we will be able to make a simple request to know the profiles which respect this information required by the recreteurs .

The same thing for ' range of salary ' we can match offers with profiles that ask for a salary lower or equal to the salary proposed by companies
Exemple of sql requet :
```
Select * From dataset1 d1 , dataset2 d2 Where d1.Rangeofsalary <= d2.RangeofSalary  AND d1.RemoteWorking=d2.RemoteWorking  AND d1.travel=d2.travel ;   
```

'![](./images/prem.png)


Until we can say that we have respected the conditions and constraints of an offer ,  but we haven't yet find a profile that corresponds to the field and the requirements that we want.


For that, it's enough to analyze the chains of characters which are in job title, job description, qualifications expected :
or example if the candidate has no experience and the company wants to hire a young graduate, then we will look in the column description of education history

* We will transform the text of education history (description), job description and qualification expected to several words.
* we will remove all the words that are not significant from the 3 columns .
* We will search in the words of the job description and the qualifications which are similar in the description Education column for the profile : so we can know if there is a common point between the offer and the profile or not.
* the ideal profile then will be the one that a maximum of common words between jobs and profile.


But if we look for a candidate who has experience, then we have four more columns that will give us information about this profile:
career path (title, company name, description, industry)

So here the skills and qualifications of the profile will also be contained in these variables.
the difference is that :
* We must consider all these variables as one and we will build a bag of words containing all the words of the profile.
* We are going to extract the words from this bag that gather to the words that are quoted in the job descriptions .
* For each one of the profiles which respect the previous constraints (remote working, salary ..) each one will have a % of connection with each of the offers according to the frequency of the common words.

still the ideal profile will be the one of which several words of the offer are in his bag, the one with high frequency : 
for example if the offer contains development of a website, and the candidate to already develop a website before and mentioned that in description of career path we will then have several words together that will increase the chance that it is the ideal profile.

3. what can we still do more with this data ?

Let us now turn to the solution that comes to the mind of a mathematician which is a little different and complicated than the first, all depending on our observations and the information that want be extracted.

We realize that we can do a study of `prediction` of the best profile based on the fact that a person who works at a company for a definite period of time is that he is already an ideal person for this mission described in carrer path, and it turns out that in the other table there are companies looking for profiles for similar missions, so they would be very happy to hire someone who has already worked in the field.

We can also predict the institutions or universities that form the best profile for a given mission, so we can at least know the level of knowledge a profile according to his education .

the problem here is that we do not have a target variable that informs us if the candidate was perfect or not, but in my opinion we can first define a variable which is based on the duration that the candidate has remained in a business, a difference between end date and start date in days or months, which can give us show his experience in a figure, and the candidate is consider good if he has spent a lot of time in a career.

for this we can introduce a prediction model based on linear regression while minimizing the error.
we will take into account that the text variables will be transformed into digital which express their meanings of words (the frequency of a certain word that we want to predict ..) or we will consider that the missions are categorials, and we will define them , of course it will be after a big study of text mining.

however this solution ignores the inexperienced candidates, we can think of such steps if we have a lot of data in our database and if we look for experienced profiles, as well as the duration of an experiment can not always given a negative meaning or positive, as it has a great complixity, that's why I prefer to go on the first solution


# About job description classification analysis in pyth
In this exercice ,I named the dataset as data I used jupyter.

please to find in the jupyter notebook named DataScience_test .
